FROM openjdk:17-alpine

WORKDIR /app

COPY target/app.jar .

ENTRYPOINT ["java","-jar","app.jar"]