## Playnance test

This is a Playnance test assignment winnings statistics microservice.

### Prerequisites

To build and run this project, you will need the following:

- Java 11 
- Maven 3.6.1

### Getting Started

1. Clone the repository:
```
git clone https://gitlab.com/xerks/playnance-test-assignment.git
```

2. Build the project:
```bash
./mvnw clean install
```

3. Set the following environment variables:
```
export APP_SYNC_DURATION_IN_SECONDS=PT60S
export APP_DATA_ORDER=DESC
```

The environment variable `APP_SYNC_DURATION_IN_SECONDS` sets the interval to sync data to in_memory storage.
(default: PT60S)
The environment variable `APP_DATA_ORDER=DESC` sets the sort order.
(default: DESC)

You can use config controller to update params on-fly

get current sync duration:
```bash
curl  http://localhost:8080/api/v1/config/sync
```
set new sync duration use:
```bash
curl  --request PUT 'http://localhost:8080/api/v1/config/sync?duration=PT10S'

```
get current sync duration:
```bash
curl  http://localhost:8080/api/v1/config/sort
```
set new sync duration:
```bash
curl  --request PUT 'http://localhost:8080/api/v1/config/sort?direction=ASC'
```

4. Build the project:
```bash
./mvnw spring-boot:run
```
5. Run the following command to build the Docker image:
```bash
docker build -t playnance-test .
```

6. Run the following command to start the containers defined in the Docker Compose file:
```bash
docker-compose up
```

7. The `playnance-test-assignment` endpoints :
```bash
curl  http://localhost:8080/api/v1/users
```
example response
```
[
    {
        "userId": "user1",
        "totalBalance": 12.66,
        "totalWinningsCount": 3
    },
    {
        "userId": "user10",
        "totalBalance": 2.21,
        "totalWinningsCount": 1
    }
]
```
```bash
curl  http://localhost:8080/api/v1/users/{userId}
```
example response
```
{
    "currentBalance": 328.37,
    "winningCount": 80,
    "partnerStats": [
        {
            "partnerId": "partnerA",
            "balance": 121.38,
            "winningsCount": 30
        },
        {
            "partnerId": "partnerB",
            "balance": 69.74,
            "winningsCount": 27
        },
        {
            "partnerId": "partnerC",
            "balance": 136.74,
            "winningsCount": 26
        }
    ]
}
```
```bash
curl  http://localhost:8080/api/v1/users/{userId}/partner/{partnerId}
```
example response
```
{
    "currentBalance": 401.35,
    "winningCount": 102,
    "partnerStats": {
        "partnerId": "partnerC",
        "balance": 236.71,
        "winningsCount": 40
    }
}
```
```bash
curl  http://localhost:8080/api/v1/partners
```
example response
```
[
    {
        "partnerId": "partnerB",
        "totalBalance": 632.06
    },
    {
        "partnerId": "partnerA",
        "totalBalance": 1209.34
    },
    {
        "partnerId": "partnerC",
        "totalBalance": 1594.10
    }
]
```
```bash
curl  http://localhost:8080/api/v1/partners/{partnerId}
```
example response
```
{
    "totalBalance": 1316.09,
    "currentFeePercentage": 50.0
}

```

8. Run the following command to stop and remove the containers:
```bash
docker-compose down
```