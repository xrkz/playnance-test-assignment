package com.playnance.assignment;

import com.playnance.assignment.config.MySQLContainerExtension;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringJUnitConfig(MySQLContainerExtension.class)
@RequiredArgsConstructor
@SpringBootTest
class AssignmentApplicationTests {
    private final ApplicationContext context;

    @Test
    void contextLoads() {
        assertNotNull(context);
    }

}
