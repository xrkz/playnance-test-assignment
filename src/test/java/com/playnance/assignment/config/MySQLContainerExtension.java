package com.playnance.assignment.config;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public class MySQLContainerExtension implements BeforeAllCallback , AfterAllCallback {

    private static final String MYSQL_IMAGE = "mysql:8.3.0";
    private static final String MYSQL_DATABASE = "winnings";
    private static final String MYSQL_USER = "username";
    private static final String MYSQL_PASSWORD = "password";

    private static MySQLContainer mysqlContainer;

    @Override
    public void beforeAll(ExtensionContext context) {
        if (mysqlContainer == null) {
            mysqlContainer = new MySQLContainer<>(DockerImageName.parse(MYSQL_IMAGE))
                    .withDatabaseName(MYSQL_DATABASE)
                    .withUsername(MYSQL_USER)
                    .withPassword(MYSQL_PASSWORD)
                    .withExposedPorts(3306)
                    .withReuse(true);

            mysqlContainer.start();

            System.setProperty("spring.datasource.url", mysqlContainer.getJdbcUrl());
            System.setProperty("spring.datasource.username", mysqlContainer.getUsername());
            System.setProperty("spring.datasource.password", mysqlContainer.getPassword());

            Runtime.getRuntime().addShutdownHook(new Thread(mysqlContainer::stop));
        }
    }

    @Override
    public void afterAll(ExtensionContext extensionContext) {
        mysqlContainer.stop();
    }
}
