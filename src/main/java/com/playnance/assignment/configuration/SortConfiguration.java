package com.playnance.assignment.configuration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

@Getter
@Setter
@RequiredArgsConstructor
@Configuration
public class SortConfiguration {

    @Value("${app.data.sort.order:DESC}")
    private Sort.Direction direction;

    public Sort.Direction getDirection() {
        return direction;
    }
}
