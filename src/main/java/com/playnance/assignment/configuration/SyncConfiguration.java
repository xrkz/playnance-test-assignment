package com.playnance.assignment.configuration;

import com.playnance.assignment.cron.SyncDataJob;
import com.playnance.assignment.services.CacheService;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Getter
@Setter
@EnableScheduling
@Configuration
public class SyncConfiguration {

    @DurationUnit(ChronoUnit.SECONDS)
    @Value("${app.sync.durationInSeconds:PT60S}")
    private Duration syncDurationInSec;


    @Bean
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(10);
        return taskScheduler;
    }

    @Bean
    public SyncDataJob syncDataJob(CacheService cacheService, SyncConfiguration syncConfiguration){
        return new SyncDataJob(cacheService, syncConfiguration, taskScheduler());
    }
}
