package com.playnance.assignment.configuration;

import com.playnance.assignment.entity.Partner;
import com.playnance.assignment.entity.User;
import com.playnance.assignment.entity.Winning;
import com.playnance.assignment.entity.WinningId;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TemporaryStorageConfiguration {

    @Bean
    public Map<String, Map<WinningId, Winning>> temporaryWinningStorageWrapper() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    public Map<String, Partner> temporaryPartnerStorage() {
        return new LinkedHashMap<>();
    }

    @Bean
    public Map<String, User> temporaryUserStorage() {
        return new LinkedHashMap<>();
    }
}
