package com.playnance.assignment.mapper;

import com.playnance.assignment.dto.PartnerStatsDto;
import com.playnance.assignment.entity.Partner;
import com.playnance.assignment.entity.User;
import com.playnance.assignment.entity.Winning;
import com.playnance.assignment.models.WinningDto;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import static org.mapstruct.MappingConstants.ComponentModel.SPRING;
import org.mapstruct.Named;

@Mapper(componentModel = SPRING, imports = {BigDecimal.class})
public interface WinningMapper {

    @Mapping(target = "user", source = ".", qualifiedByName = "mapUser")
    @Mapping(target = "partner", source = ".", qualifiedByName = "mapPartner")
    @Mapping(target = "totalWinningAmount", source = "winningAmount")
    @Mapping(target = "userWinningAmount", source = ".", qualifiedByName = "mapUserBalance")
    @Mapping(target = "partnerWinningAmount", source = ".", qualifiedByName = "mapPartnerBalance")
    @Mapping(target = "winningCount", expression = "java(1L)")
    Winning mapToWinning(WinningDto source);

    @Mapping(target = "partnerId", source = "partner.partnerId")
    @Mapping(target = "balance", source = "partnerWinningAmount")
    @Mapping(target = "winningsCount", source = "winningCount")
    PartnerStatsDto mapPartnerStats(Winning winning);


    @Named("mapUser")
    @Mapping(target = "userId", source = "user")
    @Mapping(target = "totalBalance", source = ".", qualifiedByName = "mapUserBalance")
    @Mapping(target = "totalWinningsCount", expression = "java(1L)")
    User mapUser(WinningDto source);

    @Named("mapUserBalance")
    default BigDecimal mapUserBalance(WinningDto source) {
        var feePercentage = BigDecimal.valueOf(source.getFeePercentage());
        var feeMultiplier = BigDecimal.ONE.subtract(feePercentage.divide(BigDecimal.valueOf(100.0), RoundingMode.HALF_UP));
        return source.getWinningAmount().multiply(feeMultiplier);
    }

    @Named("mapPartner")
    @Mapping(target = "partnerId", source = "partner")
    @Mapping(target = "currentFeePercentage", source = "feePercentage")
    @Mapping(target = "totalBalance", source = ".", qualifiedByName = "mapPartnerBalance")
    Partner mapPartner(WinningDto source);

    @Named("mapPartnerBalance")
    default BigDecimal mapPartnerBalance(WinningDto source) {
        var winningAmount = source.getWinningAmount();
        var feePercentage = BigDecimal.valueOf(source.getFeePercentage());

        return winningAmount.multiply(feePercentage).divide(BigDecimal.valueOf(100.0), RoundingMode.HALF_UP);
    }
}
