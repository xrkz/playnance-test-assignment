package com.playnance.assignment.mapper;

import com.playnance.assignment.dto.PartnerBalanceResponse;
import com.playnance.assignment.dto.PartnerCommissionResponse;
import com.playnance.assignment.entity.Partner;
import org.mapstruct.Mapper;
import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface PartnerMapper {

    PartnerBalanceResponse mapToPartnerBalance(Partner partner);

    PartnerCommissionResponse mapToPartnerCommission(Partner partner);
}
