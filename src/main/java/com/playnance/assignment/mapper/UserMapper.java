package com.playnance.assignment.mapper;

import com.playnance.assignment.dto.UserBalanceAllPartnersResponse;
import com.playnance.assignment.dto.UserBalanceResponse;
import com.playnance.assignment.dto.UserPartnerBalanceResponse;
import com.playnance.assignment.entity.User;
import com.playnance.assignment.entity.Winning;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING, uses = {WinningMapper.class})
public interface UserMapper {

    UserBalanceResponse mapToUserBalanceResponse(User source);

    @Mapping(target = "currentBalance", source = "user.totalBalance")
    @Mapping(target = "winningCount", source = "user.totalWinningsCount")
    @Mapping(target = "partnerStats", source = "winning")
    UserBalanceAllPartnersResponse mapToUserBalanceAllPartnersResponse(User user, List<Winning> winning);

    @Mapping(target = "currentBalance", source = "user.totalBalance")
    @Mapping(target = "winningCount", source = "user.totalWinningsCount")
    @Mapping(target = "partnerStats", source = ".")
    UserPartnerBalanceResponse mapToUserPartnerBalanceResponse(Winning source);

}
