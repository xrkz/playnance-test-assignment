package com.playnance.assignment;

import com.playnance.assignment.services.KafkaConsumer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@RequiredArgsConstructor
@SpringBootApplication
public class AssignmentApplication implements CommandLineRunner {

	private final KafkaConsumer consumer;

	public static void main(String[] args) {

		SpringApplication.run(AssignmentApplication.class, args);
	}

	@Override
	public void run(String... args) {

		consumer.start();
	}
}
