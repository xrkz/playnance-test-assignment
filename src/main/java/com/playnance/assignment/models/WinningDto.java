package com.playnance.assignment.models;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
@ToString
public class WinningDto {

    String user; // the winning user ID
    BigDecimal winningAmount; // amount he won
    String partner; // ID of a partner on whose site the user won
    float feePercentage; // fee in percents the partner gets from each winning amount
    long timestamp; // event timestamp
    // Kafka meta data
    String key; // Kafka message key - represents a service name which produced the event to Kafka
    String topic; // Kafka topic from where the event was consumed
    int partition; // Kafka partition
    long offset; // Kafka message offset




}
