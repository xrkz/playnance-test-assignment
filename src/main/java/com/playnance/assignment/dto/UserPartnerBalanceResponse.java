package com.playnance.assignment.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserPartnerBalanceResponse {
    private BigDecimal currentBalance;
    private long winningCount;
    private PartnerStatsDto partnerStats;

}
