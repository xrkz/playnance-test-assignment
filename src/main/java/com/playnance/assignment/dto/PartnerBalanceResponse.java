package com.playnance.assignment.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@Builder
@Data
public class PartnerBalanceResponse {
    private String partnerId;
    private BigDecimal totalBalance;
}
