package com.playnance.assignment.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@Builder
@Data
public class UserBalanceResponse {
    private String userId;
    private BigDecimal totalBalance;
    private long totalWinningsCount;
}
