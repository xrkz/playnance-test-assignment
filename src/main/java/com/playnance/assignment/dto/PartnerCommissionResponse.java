package com.playnance.assignment.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PartnerCommissionResponse {
    private BigDecimal totalBalance;
    private float currentFeePercentage;
}
