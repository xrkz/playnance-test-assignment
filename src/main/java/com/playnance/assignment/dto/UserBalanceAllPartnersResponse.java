package com.playnance.assignment.dto;

import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@Builder
@Data
public class UserBalanceAllPartnersResponse {
    private BigDecimal currentBalance;
    private long winningCount;
    private List<PartnerStatsDto> partnerStats;
}
