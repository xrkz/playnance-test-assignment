package com.playnance.assignment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Data
public class PartnerStatsDto{
    private String partnerId;
    private BigDecimal balance;
    private long winningsCount;
}
