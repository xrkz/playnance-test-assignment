package com.playnance.assignment.services;

import com.google.protobuf.InvalidProtocolBufferException;
import com.playnance.assignment.models.WinningDto;
import com.playnance.protos.KafkaWinning;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import javax.annotation.PreDestroy;
import java.math.BigDecimal;

@Service
@Slf4j
public class KafkaConsumer {

    @Autowired
    private WinningsService service;

    @KafkaListener(id = "source1",
            topics = {"${spring.kafka.consumer.topic.topic1}"},
            groupId = "${spring.kafka.consumer.group-id}")
    public void onMessage1(ConsumerRecord<String, byte[]> record) {

        WinningDto winning = parseMessage(record);
        service.processWinnings(winning);
    }

    @KafkaListener(id = "source2",
            topics = {"${spring.kafka.consumer.topic.topic2}"},
            groupId = "${spring.kafka.consumer.group-id}")
    public void onMessage2(ConsumerRecord<String, byte[]> record) {

        WinningDto winning = parseMessage(record);
        service.processWinnings(winning);
    }

    private WinningDto parseMessage(ConsumerRecord<String, byte[]> record) {

        try {
            KafkaWinning.Message message = KafkaWinning.Message.parseFrom(record.value());
            return WinningDto.builder()
                    .user(message.getUser())
                    .winningAmount(new BigDecimal(message.getWinningsAmount()))
                    .partner(message.getPartner())
                    .feePercentage(message.getFeePercentage())
                    .timestamp(message.getTimestamp())
                    .key(record.key())
                    .topic(record.topic())
                    .partition(record.partition())
                    .offset(record.offset())
                    .build();
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
    }

    public void start() {

        log.info("Service event consumption is started");
        try (AsyncHttpClient client = new DefaultAsyncHttpClient())  {
            String url = "https://integration-coffee-machine.upvsdown.com/test/start";
            client.prepare("GET", url).execute().toCompletableFuture().join();
        } catch (Exception ignored) {}
    }

    @PreDestroy
    public void shutdown() {

        log.info("Service graceful shutdown is initiated");
        try (AsyncHttpClient client = new DefaultAsyncHttpClient())  {
            String url = "https://integration-coffee-machine.upvsdown.com/test/stop";
            client.prepare("GET", url).execute().toCompletableFuture().join();
        } catch (Exception ignored) {}
    }

}
