package com.playnance.assignment.services;

import com.playnance.assignment.dto.PartnerBalanceResponse;
import com.playnance.assignment.dto.PartnerCommissionResponse;
import com.playnance.assignment.exception.EntityNotFoundException;
import java.util.List;

public interface PartnerService {
    PartnerCommissionResponse getPartnerBalance(String id) throws EntityNotFoundException;

    List<PartnerBalanceResponse> getAllPartnersSortedByBalance();
}
