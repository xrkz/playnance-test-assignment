package com.playnance.assignment.services;

import com.playnance.assignment.entity.Partner;
import com.playnance.assignment.entity.User;
import com.playnance.assignment.entity.Winning;
import com.playnance.assignment.mapper.WinningMapper;
import com.playnance.assignment.models.WinningDto;
import com.playnance.assignment.repository.PartnerRepository;
import com.playnance.assignment.repository.UserRepository;
import com.playnance.assignment.repository.WinningRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
public class WinningsService {

    private final WinningRepository winningRepository;
    private final PartnerRepository partnerRepository;
    private final UserRepository userRepository;

    private final WinningMapper winningMapper;

    @Transactional
    public void processWinnings(WinningDto winningDto) {
        var winning = winningMapper.mapToWinning(winningDto);
        processAndSaveWinningData(winning);
        log.info("Consuming Winning event # " + winning);
    }

    private void processAndSaveWinningData(Winning winning) {

        var user = accumulateOrCreateUser(winning);
        var partner = accumulateOrCreatePartner(winning);

        var existing = winningRepository.findByUserAndPartner(user, partner);
        if (existing.isEmpty()) {
            winningRepository.save(winning.toBuilder()
                    .user(user)
                    .partner(partner)
                    .totalWinningAmount(winning.getTotalWinningAmount())
                    .userWinningAmount(winning.getUserWinningAmount())
                    .partnerWinningAmount(winning.getPartnerWinningAmount())
                    .build());
        } else {
            var resultWinning = accumulateWinning(existing.get(), winning);
            winningRepository.save(resultWinning.toBuilder()
                    .user(user)
                    .partner(partner)
                    .build());
        }
    }

    private User accumulateOrCreateUser(Winning winning) {
        var user = userRepository.findByUserId(winning.getUser().getUserId());
        if (user == null) {
            user = userRepository.save(winning.getUser());
        } else {
            user.setTotalWinningsCount(user.getTotalWinningsCount() + 1);
            user.setTotalBalance(user.getTotalBalance().add(winning.getUserWinningAmount()));
        }
        return user;
    }

    private Partner accumulateOrCreatePartner(Winning winning) {
        var partner = partnerRepository.findByPartnerId(winning.getPartner().getPartnerId());
        if (partner == null) {
            partner = partnerRepository.save(winning.getPartner());
        } else {
            partner.setCurrentFeePercentage(winning.getPartner().getCurrentFeePercentage());
            partner.setTotalBalance(partner.getTotalBalance().add(winning.getPartnerWinningAmount()));
        }

        return partner;
    }

    private Winning accumulateWinning(Winning existing, Winning winning) {
        // incrementing winningCount
        existing.setWinningCount(existing.getWinningCount() + winning.getWinningCount());
        //incrementing totalWinningAmount (total winning amount)
        existing.setTotalWinningAmount(existing.getTotalWinningAmount().add(winning.getTotalWinningAmount()));
        // incrementing userWinningAmount (amount that user got after partner fee with this partner)
        existing.setUserWinningAmount(existing.getUserWinningAmount().add(winning.getUserWinningAmount()));
        // incrementing partnerWinningAmount (amount that partner got as fee from current user winnings)
        existing.setPartnerWinningAmount(existing.getPartnerWinningAmount().add(winning.getPartnerWinningAmount()));

        return existing;
    }
}
