package com.playnance.assignment.services;

import com.playnance.assignment.entity.Partner;
import com.playnance.assignment.entity.User;
import com.playnance.assignment.entity.Winning;
import java.util.List;

public interface CacheService {
    List<Partner> getAllPartners();

    Partner getPartnerById(String id);

    List<User> getAllUsers();

    User getUserById(String id);

    Winning getUserWinningStatsByPartner(String userId, String partnerId);

    List<Winning> getUserWinning(String userId);

    void syncCacheData();
}
