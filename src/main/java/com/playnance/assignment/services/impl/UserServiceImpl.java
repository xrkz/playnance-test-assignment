package com.playnance.assignment.services.impl;

import com.playnance.assignment.dto.UserBalanceAllPartnersResponse;
import com.playnance.assignment.dto.UserBalanceResponse;
import com.playnance.assignment.dto.UserPartnerBalanceResponse;
import com.playnance.assignment.exception.EntityNotFoundException;
import com.playnance.assignment.mapper.UserMapper;
import com.playnance.assignment.services.CacheService;
import com.playnance.assignment.services.UserService;
import static java.lang.String.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final CacheService cacheService;
    private final UserMapper userMapper;

    @Override
    public List<UserBalanceResponse> getAllUsersSortedByBalance() {
        var users = cacheService.getAllUsers();

        return users.stream()
                .map(userMapper::mapToUserBalanceResponse)
                .collect(Collectors.toList());
    }

    @Override
    public UserBalanceAllPartnersResponse getUserAllPartnersStats(String id) throws EntityNotFoundException {
        var user = Optional.ofNullable(cacheService.getUserById(id))
                .orElseThrow(() -> new EntityNotFoundException(format("User with id [%s] not found", id)));

        var winnings = cacheService.getUserWinning(user.getUserId());

        return userMapper.mapToUserBalanceAllPartnersResponse(user, winnings);
    }

    @Override
    public UserPartnerBalanceResponse getUserPartnersStats(String userId, String partnerId) throws EntityNotFoundException {
        var winningStats = Optional.ofNullable(cacheService.getUserWinningStatsByPartner(userId, partnerId))
                .orElseThrow(() -> new EntityNotFoundException(format("Winning for User [%s] and Partner [%s] not found", userId, partnerId)));

        return userMapper.mapToUserPartnerBalanceResponse(winningStats);
    }
}
