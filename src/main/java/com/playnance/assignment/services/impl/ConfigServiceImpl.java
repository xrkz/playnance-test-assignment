package com.playnance.assignment.services.impl;

import com.playnance.assignment.configuration.SortConfiguration;
import com.playnance.assignment.configuration.SyncConfiguration;
import com.playnance.assignment.cron.SyncDataJob;
import com.playnance.assignment.services.ConfigService;
import java.time.Duration;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ConfigServiceImpl implements ConfigService {

    private final SortConfiguration sortConfiguration;
    private final SyncConfiguration syncConfiguration;
    private final SyncDataJob syncDataJob;


    @Override
    public Sort.Direction getOrderDirection() {
        return sortConfiguration.getDirection();
    }


    @Override
    public void setOrderDirection(Sort.Direction direction) {
        sortConfiguration.setDirection(direction);
    }

    @Override
    public Duration getSyncDuration() {
        return syncConfiguration.getSyncDurationInSec();
    }

    @Override
    public void setSyncDuration(Duration duration) {
        syncConfiguration.setSyncDurationInSec(duration);
        syncDataJob.rescheduleTask();

    }
}
