package com.playnance.assignment.services.impl;

import com.playnance.assignment.configuration.SortConfiguration;
import com.playnance.assignment.entity.Partner;
import com.playnance.assignment.entity.User;
import com.playnance.assignment.entity.Winning;
import com.playnance.assignment.entity.WinningId;
import com.playnance.assignment.repository.PartnerRepository;
import com.playnance.assignment.repository.UserRepository;
import com.playnance.assignment.repository.WinningRepository;
import com.playnance.assignment.services.CacheService;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CacheServiceImpl implements CacheService {

    private final UserRepository userRepository;
    private final PartnerRepository partnerRepository;
    private final WinningRepository winningRepository;

    private final Map<String, User> temporaryUserStorage;
    private final Map<String, Partner> temporaryPartnerStorage;
    private final Map<String, Map<WinningId, Winning>> temporaryWinningStorageWrapper;

    private final SortConfiguration sortConfiguration;

    @Override
    public List<User> getAllUsers() {
        return new LinkedList<>(temporaryUserStorage.values());
    }

    @Override
    public User getUserById(String id) {
        return temporaryUserStorage.get(id);
    }

    @Override
    public List<Partner> getAllPartners() {
        return new LinkedList<>(temporaryPartnerStorage.values());
    }

    @Override
    public Partner getPartnerById(String id) {
        return temporaryPartnerStorage.get(id);
    }

    @Override
    public Winning getUserWinningStatsByPartner(String userId, String partnerId) {
        var user = temporaryUserStorage.get(userId);
        var partner = temporaryPartnerStorage.get(partnerId);
        var key = new WinningId(user.getUserId(), partner.getPartnerId());

        return temporaryWinningStorageWrapper.get(userId).get(key);
    }

    @Override
    public List<Winning> getUserWinning(String userId) {
        return new ArrayList<>(temporaryWinningStorageWrapper.get(userId).values());
    }

    @Override
    public void syncCacheData() {
        var sortDirection = sortConfiguration.getDirection();
        var userMap = getUsers(sortDirection);
        var partnerMap = getPartners(sortDirection);
        var winningMap = getWinnings();

        temporaryUserStorage.clear();
        temporaryUserStorage.putAll(userMap);
        temporaryPartnerStorage.clear();
        temporaryPartnerStorage.putAll(partnerMap);
        temporaryWinningStorageWrapper.clear();
        temporaryWinningStorageWrapper.putAll(winningMap);
    }

    private Map<String, Map<WinningId, Winning>> getWinnings() {
        return winningRepository.findAll().stream()
                .collect(Collectors.groupingBy(
                        winning -> winning.getUser().getUserId(),
                        ConcurrentHashMap::new,
                        Collectors.toMap(
                                winning -> new WinningId(winning.getUser().getUserId(), winning.getPartner().getPartnerId()),
                                winning -> winning
                        )
                ));
    }

    private LinkedHashMap<String, Partner> getPartners(Sort.Direction sortDirection) {
        var partners = partnerRepository.findAll(Sort.by(sortDirection, Partner.Fields.totalBalance));

        return partners.stream()
                .collect(Collectors.toMap(
                        Partner::getPartnerId,
                        partner -> partner,
                        (existing, replacement) -> replacement,
                        LinkedHashMap::new
                ));
    }

    private LinkedHashMap<String, User> getUsers(Sort.Direction sortDirection) {
        var users = userRepository.findAll(Sort.by(sortDirection, User.Fields.totalBalance));
        return users.stream()
                .collect(Collectors.toMap(
                        User::getUserId,
                        user -> user,
                        (existing, replacement) -> replacement,
                        LinkedHashMap::new
                ));
    }
}
