package com.playnance.assignment.services.impl;

import com.playnance.assignment.dto.PartnerBalanceResponse;
import com.playnance.assignment.dto.PartnerCommissionResponse;
import com.playnance.assignment.exception.EntityNotFoundException;
import com.playnance.assignment.mapper.PartnerMapper;
import com.playnance.assignment.services.CacheService;
import com.playnance.assignment.services.PartnerService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class PartnerServiceImpl implements PartnerService {

    private final CacheService cacheService;
    private final PartnerMapper partnerMapper;

    @Override
    public PartnerCommissionResponse getPartnerBalance(String id) throws EntityNotFoundException {
        var partner = Optional.ofNullable(cacheService.getPartnerById(id))
                .orElseThrow(() -> new EntityNotFoundException(String.format("Partner with id [%s] not found", id)));

        return partnerMapper.mapToPartnerCommission(partner);
    }

    @Override
    public List<PartnerBalanceResponse> getAllPartnersSortedByBalance() {
        var partners = cacheService.getAllPartners();

        return partners.stream()
                .map(partnerMapper::mapToPartnerBalance)
                .collect(Collectors.toList());
    }
}
