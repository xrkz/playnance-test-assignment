package com.playnance.assignment.services;

import com.playnance.assignment.dto.UserBalanceAllPartnersResponse;
import com.playnance.assignment.dto.UserBalanceResponse;
import com.playnance.assignment.dto.UserPartnerBalanceResponse;
import com.playnance.assignment.exception.EntityNotFoundException;
import java.util.List;

public interface UserService {
    List<UserBalanceResponse> getAllUsersSortedByBalance();

    UserBalanceAllPartnersResponse getUserAllPartnersStats(String id) throws EntityNotFoundException;

    UserPartnerBalanceResponse getUserPartnersStats(String userId, String partnerId) throws EntityNotFoundException;
}
