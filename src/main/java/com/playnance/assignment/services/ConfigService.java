package com.playnance.assignment.services;

import java.time.Duration;
import org.springframework.data.domain.Sort;

public interface ConfigService {

    Sort.Direction getOrderDirection();

    void setOrderDirection(Sort.Direction direction);

    Duration getSyncDuration();

    void setSyncDuration(Duration order);

}
