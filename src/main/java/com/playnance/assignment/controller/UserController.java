package com.playnance.assignment.controller;

import com.playnance.assignment.dto.UserBalanceAllPartnersResponse;
import com.playnance.assignment.dto.UserBalanceResponse;
import com.playnance.assignment.dto.UserPartnerBalanceResponse;
import com.playnance.assignment.exception.EntityNotFoundException;
import com.playnance.assignment.services.UserService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {
    private final UserService userService;

    @GetMapping
    public List<UserBalanceResponse> getAllUsers() {
        return userService.getAllUsersSortedByBalance();
    }

    @GetMapping("/{id}")
    public UserBalanceAllPartnersResponse getUserBalanceAllPartners(@PathVariable String id) throws EntityNotFoundException {
        return userService.getUserAllPartnersStats(id);
    }

    @GetMapping("/{userId}/partner/{partnerId}")
    public UserPartnerBalanceResponse getUserBalancePerPartner(
            @PathVariable String userId,
            @PathVariable String partnerId) throws EntityNotFoundException {
        return userService.getUserPartnersStats(userId, partnerId);
    }
}
