package com.playnance.assignment.controller;

import com.playnance.assignment.dto.PartnerBalanceResponse;
import com.playnance.assignment.dto.PartnerCommissionResponse;
import com.playnance.assignment.exception.EntityNotFoundException;
import com.playnance.assignment.services.PartnerService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/partners")
public class PartnerController {
    private final PartnerService partnerService;

    @GetMapping
    public List<PartnerBalanceResponse> getAllPartnersSortedByBalance() {
        return partnerService.getAllPartnersSortedByBalance();
    }

    @GetMapping("/{id}")
    public PartnerCommissionResponse getPartnerBalanceAndFee(@PathVariable String id) throws EntityNotFoundException {
        return partnerService.getPartnerBalance(id);
    }
}
