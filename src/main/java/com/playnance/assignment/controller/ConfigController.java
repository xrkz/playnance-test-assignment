package com.playnance.assignment.controller;


import com.playnance.assignment.services.ConfigService;
import java.time.Duration;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.data.domain.Sort.Direction;
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/config")
public class ConfigController {

    private final ConfigService configService;

    @GetMapping("/sync")
    public ResponseEntity<Duration> getSyncDuration() {
        return ResponseEntity.ok(configService.getSyncDuration());
    }

    @PutMapping("/sync")
    public ResponseEntity<Void> setSyncDuration(@RequestParam Duration duration) {
        configService.setSyncDuration(duration);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/sort")
    public ResponseEntity<Direction> getSortOrder() {
        return ResponseEntity.ok(configService.getOrderDirection());
    }

    @PutMapping("/sort")
    public ResponseEntity<Void> setSortOrder(@RequestParam Direction direction) {
        configService.setOrderDirection(direction);
        return ResponseEntity.noContent().build();
    }

}
