package com.playnance.assignment.event;

import com.playnance.assignment.services.CacheService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class AppStartListener {

    private final CacheService cacheService;

    @EventListener(ApplicationStartedEvent.class)
    public void recoverCache() {
        cacheService.syncCacheData();
    }
}
