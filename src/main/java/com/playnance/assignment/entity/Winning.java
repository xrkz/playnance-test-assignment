package com.playnance.assignment.entity;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(WinningId.class)
@Entity
@Table(name = "winnings")
public class Winning {
    @Id
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id", insertable = false, nullable = false)
    private User user;

    @Id
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "partner_id", insertable = false, nullable = false)
    private Partner partner;

    @Column(name = "total_win_amount")
    private BigDecimal  totalWinningAmount;

    @Column(name = "user_win_amount")
    private BigDecimal  userWinningAmount;

    @Column(name = "partner_win_amount")
    private BigDecimal  partnerWinningAmount;

    @Builder.Default
    @Column(name = "winning_count")
    private long winningCount = 1;
}
