package com.playnance.assignment.exception;

import java.io.Serializable;

public class EntityNotFoundException extends Exception implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String MESSAGE = "NOT FOUND => ";

    public EntityNotFoundException(String message) {
        super(MESSAGE + message);
    }

}
