package com.playnance.assignment.repository;

import com.playnance.assignment.entity.Partner;
import com.playnance.assignment.entity.User;
import com.playnance.assignment.entity.Winning;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WinningRepository extends JpaRepository<Winning, Long> {
    Optional<Winning> findByUserAndPartner(User user, Partner partner);
}
