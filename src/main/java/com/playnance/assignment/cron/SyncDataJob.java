package com.playnance.assignment.cron;

import com.playnance.assignment.configuration.SyncConfiguration;
import com.playnance.assignment.services.CacheService;
import java.util.concurrent.ScheduledFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

@Slf4j
@RequiredArgsConstructor
public class SyncDataJob implements SchedulingConfigurer {

    private final CacheService cacheService;
    private final SyncConfiguration syncConfiguration;
    private final TaskScheduler taskScheduler;
    private ScheduledFuture<?> scheduledFuture;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setTaskScheduler(taskScheduler);
        scheduledFuture = taskScheduler.scheduleAtFixedRate(cacheService::syncCacheData, syncConfiguration.getSyncDurationInSec().toMillis());
    }

    public void rescheduleTask() {
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
        scheduledFuture = taskScheduler.scheduleAtFixedRate(cacheService::syncCacheData, syncConfiguration.getSyncDurationInSec().toMillis());
    }
}
